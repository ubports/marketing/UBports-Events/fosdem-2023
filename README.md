# Fosdem 2023

https://fosdem.org/2023/schedule/event/lomiri/


Lomiri Mobile Linux in Desktop mode
Lomiri and the myth of the pocket size desktop computer
 Track: FOSS on Mobile Devices devroom
 Room: UB4.136
 Day: Saturday
 Start: 10:30
 End: 10:55
 Video only: ub4136
 Chat: Join the conversation!

The commonalization of desktop, phone & tablet, with the goal of providing the most user-friendly shell to the hands of users. With years of development behind it and a strong community of enthusiasts around it, Lomiri is well positioned to provide a delightful experience to both power users as well as ordinary users, powered by future-proof technologies.

In this presentation he will demonstrate how over the last 5 years Ubuntu Touch convergence has evolved from vision to reality. Switching from the custom windowing protocol it used to a commonly shared one, adapting to the defacto-standard systems layer for GNU/Linux systems, software pieces that work in tandem to provide UI features such as workspaces with window snapping and resolution specific window scaling, and managing app lifecycles in a scenario-based way.

https://fosdem.org/2023/schedule/speaker/alfred_neumayer/

Alfred's slide presentation https://gitlab.com/ubports/marketing/UBports-Events/fosdem-2023/-/blob/main/Lomiri_and_the_myth_of_the_pocket_size_desktop_computer.zip

Alfred's video presentation at Fosdem https://ftp.heanet.ie/mirrors/fosdem-video/2023/UB4.136/lomiri.webm





